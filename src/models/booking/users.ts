import { Knex } from 'knex';
import * as crypto from 'crypto';

export class UsersModel {

  list(db: Knex) {
    return db('users');
  }

  getByID(db: Knex, id: number) {
    return db('users')
      .where('user_id', id)
      .andWhere('is_active', true);
  }

  getByUsername(db: Knex, usernmae: string) {
    return db('users')
      .where('username', usernmae)
      .andWhere('is_active', true);
  }

  getByRoleID(db: Knex, id: number) {
    return db('users')
      .where('role_id', id)
      .andWhere('is_active', true);
  }

  create(db: Knex, data: any) {
    const encPassword = crypto.createHash('md5').update(data.password).digest('hex');
    delete data.password;
    data.password_hash = encPassword;
    return db('users')
      .insert(data)
      .returning('*');
  }

  update(db: Knex, data: any, id: number) {
    if (data.password) {
      const encPassword = crypto.createHash('md5').update(data.password).digest('hex');
      delete data.password;
      data.password_hash = encPassword;
    }

    return db('users')
      .where('user_id', id)
      .update(data)
      .returning('*');
  }

  async updatePassword(db: Knex, data: any, id: number ,key: number) {
    console.log('data :',data);
    console.log('id :',id);
    console.log('key :',key);
    let res :any = await this.getLastCid(db,id,key)
    if (data.password) {
      const encPassword = crypto.createHash('md5').update(data.password).digest('hex');
      delete data.password;
      data.password_hash = encPassword;
    }
    if(res.length > 0) {
      return db('users')
      .where('user_id', id)
      .update(data)
      .returning('*');
    }else {
      return []
    }

  }

  getLastCid(db: Knex, id: number ,key: number) {
    return db('users').leftJoin('profiles','profiles.user_id','users.user_id')
    .where('users.user_id', id).andWhere(db.raw('Right(profiles.cid,4) = ?',[key]))
  }

  delete(db: Knex, id: number) {
    return db('users')
      .where('user_id', id)
      .delete();
  }

  doLogin(db: Knex, username: string, password: string) {
    // console.log("db : ",db);
    console.log("username : ", username);
    console.log("password : ", password);

    const encPassword = crypto.createHash('md5').update(password).digest('hex');
    console.log(encPassword);
    // let rs = db('users');
    // console.log(rs);
    // return rs;
    return db('users')
      .where('username', username)
      .andWhere('password_hash', encPassword)
      .andWhere('is_active', true);
  }

  getByHospitalID(db: Knex, id: number) {
    return db('users')
    .leftJoin('profiles', 'users.user_id', 'profiles.user_id')
    .where('profiles.hospital_id', id)
    .andWhere('users.is_active',true);
  }  
}