import { Knex } from 'knex';

export class ProfilesModel {

  list(db: Knex) {
    return db('profiles');
  }  

  getByID(db: Knex, id: any) {
    return db('profiles')
    .where('user_id', id);
  }  

  getByHospitalID(db: Knex, id: any) {
    return db('profiles')
    .where('hospital_id', id);
  } 

  getByCID(db: Knex, cid: string) {
    return db('profiles')
    .where('cid', cid);
  }  

  getSearch(db: Knex, text: string) {
    return db('profiles')
    .whereLike('fullname', `%${text}%`);
  }  

  create(db: Knex, data: any) {
    return db('profiles')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: any) {
    return db('profiles')
    .where('user_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: any) {
    return db('profiles')
      .where('user_id', id)
      .delete();
  }

}