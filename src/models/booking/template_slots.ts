import { Knex } from 'knex';

export class TemplateSlotsModel {

  list(db: Knex) {
    return db('template_slots');
  }  

  info(db: Knex) {
    return db('template_slots')
    .where('is_active',true);
  } 

  getByID(db: Knex, id: number) {
    return db('template_slots')
    .where('template_slot_id', id)
    .andWhere('is_active',true);
  }  

  getByPeriodID(db: Knex, id: number) {
    return db('template_slots')
    .where('period_id', id)
    .andWhere('is_active',true);
  } 

  getByServiceID(db: Knex, id: number) {
    return db('template_slots')
    .where('service_id', id)
    .andWhere('is_active',true);
  }  

  getByServiceTypeID(db: Knex, id: number) {
    return db('template_slots')
    .where('service_type_id', id)
    .andWhere('is_active',true);
  }  

  getSearch(db: Knex, text: string) {
    return db('template_slots')
    .whereLike('template_slot_name', `%${text}%`);
  }  

  create(db: Knex, data: any) {
    return db('template_slots')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('template_slots')
    .where('template_slot_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('template_slots')
      .where('template_slot_id', id)
      .delete();
  }

}