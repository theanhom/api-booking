import { Knex } from 'knex';

export class SlotsModel {

  list(db: Knex) {
    return db('slots')
      .where('slot_date', '>=', db.raw('CURRENT_DATE'))
  }

  info(db: Knex) {
    return db('slots')
      .where('is_active', true)
      .andWhere('slot_avialable', true)
      .where('slot_date', '>=', db.raw('CURRENT_DATE'))
  }

  getByID(db: Knex, id: number) {
    return db('slots')
      .where('slot_id', id)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw('CURRENT_DATE'))
  }


  getByPeriodID(db: Knex, id: number) {
    return db('slots')
      .where('period_id', id)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw('CURRENT_DATE'))
  }

  getByServiceID(db: Knex, id: number) {
    return db('slots')
      .where('service_id', id)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw('CURRENT_DATE'))
  }

  getByServiceTypeID(db: Knex, id: number) {
    return db('slots')
      .where('service_type_id', id)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw('CURRENT_DATE'))
  }

  getByHospitalID(db: Knex, id: number) {
    return db('slots')
      .where('hospital_id', id)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw('CURRENT_DATE'))
  }

  getLookupByHospitalID(db: Knex, id: number) {
    return db('slots')
      .where('hospital_id', id)
      .andWhere('is_active', true)
      .andWhere('slot_avialable', true)
      .where('slot_date', '>=', db.raw('CURRENT_DATE'))
  }

  getSearch(db: Knex, text: string) {
    return db('slots')
      .whereLike('slot_name', `%${text}%`)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw('CURRENT_DATE'))
  }

  create(db: Knex, data: any) {
    return db('slots')
      .insert(data)
      .returning('*');
  }

  update(db: Knex, data: any, id: number) {
    return db('slots')
      .where('slot_id', id)
      .update(data)
      .returning('*');
  }

  delete(db: Knex, id: number) {
    return db('slots')
      .where('slot_id', id)
      .delete();
  }

  getManagementSlot(db: Knex, data: any) {
    return db('slots')
      .where('slot_date', data.slot_date)
      .andWhere('hospital_id', data.hospital_id)
      .andWhere('service_id', data.service_id)
      .andWhere('is_active', true);
  }

  getLookupService(db: Knex) {
    return db('slots').select('services.service_id', 'services.service_name', 'service_types.service_type_id', 'service_types.service_type_name','service_types.note','service_types.icon_filename')
      .leftJoin('services', 'slots.service_id', 'services.service_id')
      .leftJoin('service_types', 'slots.service_type_id', 'service_types.service_type_id')
      .where('slots.slot_date', '>=', db.raw('CURRENT_DATE'))
      .andWhere('slots.is_active', true)
      .groupBy('services.service_id', 'services.service_name', 'service_types.service_type_id', 'service_types.service_type_name','service_types.icon_filename');
  }

  getLookupDateSlot(db: Knex, service_type_id: number) {
    return db('slots').select('slots.slot_id', 'slots.slot_date', 'slots.slot_name', 'slots.hospital_id', 'hospitals.hospital_name', 'periods.period_name')
      .leftJoin('hospitals', 'slots.hospital_id', 'hospitals.hospital_id')
      .leftJoin('periods', 'slots.period_id', 'periods.period_id')
      .where('slots.slot_date', '>=', db.raw('CURRENT_DATE'))
      .andWhere('slots.service_type_id', service_type_id)
      .andWhere('slots.is_active', true);
  }

  getLookupHospital(db: Knex) {
    return db('slots').select('hospitals.*')
      .leftJoin('hospitals', 'slots.hospital_id', 'hospitals.hospital_id')
      .where('slots.slot_date', '>=', db.raw('CURRENT_DATE'))
      .andWhere('slots.is_active', true);
  }

  getLookupCustomer(db: Knex, user_id: number) {
    return db('reserves').select('customers.*')
      .leftJoin('customers', 'reserves.customer_id', 'customers.customer_id')
      .where('reserves.user_id', user_id)
  }


}