import { Knex } from 'knex';

export class ReservesModel {

  list(db: Knex) {
    return db('reserves')
    .where('reserve_date','>=' ,db.raw('CURRENT_DATE'))

  }

  getByID(db: Knex, id: number) {
    return db('reserves')
      .where('reserve_id', id)
      .where('reserve_date','>=' ,db.raw('CURRENT_DATE'))
  }

  getByCustomerID(db: Knex, id: number) {
    return db('reserves')
      .where('customer_id', id)
      .where('reserve_date','>=' ,db.raw('CURRENT_DATE'))
  }

  getBySlotID(db: Knex, id: number) {
    return db('reserves')
      .where('slot_id', id)
      .where('reserve_date','>=' ,db.raw('CURRENT_DATE'))
  }

  getByUserID(db: Knex, id: number) {
    return db('reserves')
      .where('user_id', id)
      .where('reserve_date','>=' ,db.raw('CURRENT_DATE'))
  }

  getByServiceTypeID(db: Knex, id: number) {
    return db('reserves')
      .where('service_type_id', id)
      .where('reserve_date','>=' ,db.raw('CURRENT_DATE'))
  }
  getSearch(db: Knex, text: string) {
    return db('reserves')
      .whereLike('reserve_note', `%${text}%`)
      .where('reserve_date','>=' ,db.raw('CURRENT_DATE'))
  }

  create(db: Knex, data: any) {
    return db('reserves')
      .insert(data)
      .returning('*');
  }

  update(db: Knex, data: any, id: number) {
    return db('reserves')
      .where('reserve_id', id)
      .update(data)
      .returning('*');
  }

  delete(db: Knex, id: number) {
    return db('reserves')
      .where('reserve_id', id)
      .delete();
  }

  getManagementReserve(db: Knex, data: any) {
    return db('reserves').leftJoin('slots', 'slots.slot_id', 'reserves.slot_id')
      .where('reserves.reserve_date', data.reserve_date)
      .andWhere('slots.hospital_id', data.hospital_id)
      .andWhere('slots.service_id', data.service_id)
  }

  countByID(db: Knex, id: number) {
    return db('reserves')
    .count('reserve_id as total')
    .where('slot_id', id)
    // .andWhere('is_active', true);
  }

  getSlotPerPeriod(db: Knex, id: number) {
    return db('slots')
    .where('slot_id', id)
    .select('slots.total_slot_per_period as total');
  }

  setSlotUnavialable(db: Knex, id: number,data: any) {
    return db('slots')
    .where('slot_id', id)
    .update(data)
    .returning('*');
  }

  getPersonReserve(db: Knex, id: any) {
    return db('reserves')
    .where('reserves.reserve_date','>=' ,db.raw('CURRENT_DATE'))
    .andWhere('reserves.user_id', id)
  }
}