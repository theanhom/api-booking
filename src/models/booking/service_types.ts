import { Knex } from 'knex';

export class ServiceTypesModel {

  list(db: Knex) {
    return db('service_types');
  }  

  info(db: Knex) {
    return db('service_types')
    .where('is_active',true);
  }  

  getByID(db: Knex, id: number) {
    return db('service_types')
    .where('service_type_id', id)
    .andWhere('is_active',true);
  }  

  getByHospitalID(db: Knex, id: number) {
    return db('service_types')
    .where('hospital_id', id)
    .andWhere('is_active',true);
  } 

  getByServiceID(db: Knex, id: number) {
    return db('service_types')
    .where('service_id', id)
    .andWhere('is_active',true);
  }  

  getSearch(db: Knex, text: string) {
    return db('service_types')
    .whereLike('service_type_name', `%${text}%`)
    .andWhere('is_active',true);
  }  

  create(db: Knex, data: any) {
    return db('service_types')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('service_types')
    .where('service_type_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('service_types')
      .where('service_type_id', id)
      .delete();
  }

}