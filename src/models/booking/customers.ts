import { Knex } from 'knex';

export class CustomersModel {

  list(db: Knex) {
    return db('customers');
  }  

  getByID(db: Knex, id: number) {
    return db('customers')
    .where('customer_id', id);
  }  

  getSearch(db: Knex, text: string) {
    return db('customers')
    .whereLike('customer_name', `%${text}%`);
  }  

  create(db: Knex, data: any) {
    return db('customers')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('customers')
    .where('customer_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('customers')
    .where('customer_id', id)
    .delete();
  }

  getManagementCustomer(db: Knex, data:any) {
    return db('customers')
    .leftJoin('reserves', 'reserves.customer_id', 'customers.customer_id')
    .leftJoin('slots', 'slots.slot_id', 'reserves.slot_id')
    .whereLike('customer_name', `%${data.searchtext}%`)
    .andWhere('slots.hospital_id', data.hospital_id)
    .andWhere('slots.service_id', data.service_id);
  }  

}