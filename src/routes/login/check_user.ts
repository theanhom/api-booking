import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { UsersModel } from '../../models/booking/users';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

    const db: Knex = fastify.db;
    const usersModel = new UsersModel();
    fastify.get('/getByUsername/:username', async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;
        const username: string = req.params.username;
        try {
            let datas: any = await usersModel.getByUsername(db, username);
            let status: any;
            let user_id: any;
            if (datas.length > 0) {
                status = true;
                user_id = datas[0].user_id;
            } else {
                status = false;
                user_id = null
            }
            return reply.status(StatusCodes.OK)
                .send({
                    status: StatusCodes.OK,
                    ok: status,
                    results: user_id
                });
        } catch (error: any) {
            request.log.error(error);
            return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                .send({
                    status: StatusCodes.INTERNAL_SERVER_ERROR,
                    ok: false,
                    error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                });
        }
    })

    done();
}