import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { UsersModel } from '../../models/booking/users';
import { ProfilesModel } from '../../models/booking/profiles';
import { RolesModel } from '../../models/booking/roles';
import { HospitalsModel } from '../../models/booking/hospitals';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const usersModel = new UsersModel();
  const profilesModel = new ProfilesModel();
  const rolesModel = new RolesModel();
  const hospitalsModel = new HospitalsModel();

  fastify.post('/user', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }

    try {
      let datas: any = await usersModel.create(db, data);
      for (let v of datas) {
        delete v.password_hash;
      }
      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.post('/profile', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }

    try {
      let datas :any = await profilesModel.create(db,data);

      return reply.status(StatusCodes.OK)
        .send({
          status:StatusCodes.OK,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  }) 

  fastify.put('/resetPassword/:id/:key', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id: number = req.params.id;
    const key: number = req.params.key;
    const data: any = { ...req.body }

    try {
      let datas: any = await usersModel.updatePassword(db, data, id,key);
      for (let v of datas) {
        delete v.password_hash;
      }

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  done();
}