import { FastifyInstance } from "fastify";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./configs'), { prefix: '/configs' });
  fastify.register(require('./customers'), { prefix: '/customers' });
  fastify.register(require('./hospitals'), { prefix: '/hospitals' });
  // fastify.register(require('./login'), { prefix: '/login' });
  fastify.register(require('./logs'), { prefix: '/logs' });
  fastify.register(require('./periods'), { prefix: '/periods' });
  fastify.register(require('./profiles'), { prefix: '/profiles' });
  fastify.register(require('./reserves'), { prefix: '/reserves' });
  fastify.register(require('./roles'), { prefix: '/roles' });
  fastify.register(require('./service_types'), { prefix: '/service_types' });
  fastify.register(require('./services'), { prefix: '/services' });
  fastify.register(require('./slots'), { prefix: '/slots' });
  fastify.register(require('./template_slots'), { prefix: '/template_slots' });
  fastify.register(require('./users'), { prefix: '/users' });

  done();

} 
