import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { SlotsModel } from '../../models/booking/slots';
import { ServicesModel } from '../../models/booking/services';
import { HospitalsModel } from '../../models/booking/hospitals';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

    const db: Knex = fastify.db;
    const slotsModel = new SlotsModel();
    const servicesModel = new ServicesModel();
    const sospitalsModel = new HospitalsModel();

    fastify.get('/getLookupService', async (request: FastifyRequest, reply: FastifyReply) => {

        try {
            let datas: any = await slotsModel.getLookupService(db);

            return reply.status(StatusCodes.OK)
                .send({
                    status: StatusCodes.OK,
                    ok: true,
                    results: datas
                });
        } catch (error: any) {
            request.log.error(error);
            return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                .send({
                    status: StatusCodes.INTERNAL_SERVER_ERROR,
                    ok: false,
                    error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                });
        }
    })

    fastify.get('/getLookupDateSlot/:service_type_id', async (request: FastifyRequest, reply: FastifyReply) => {

        try {
            let req: any = request.params;
            let service_type_id = req.service_type_id;
            let datas: any = await slotsModel.getLookupDateSlot(db, service_type_id);

            return reply.status(StatusCodes.OK)
                .send({
                    status: StatusCodes.OK,
                    ok: true,
                    results: datas
                });
        } catch (error: any) {
            request.log.error(error);
            return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                .send({
                    status: StatusCodes.INTERNAL_SERVER_ERROR,
                    ok: false,
                    error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                });
        }
    })

    fastify.get('/getLookupHospital', async (request: FastifyRequest, reply: FastifyReply) => {

        try {
            let datas: any = await slotsModel.getLookupHospital(db);

            return reply.status(StatusCodes.OK)
                .send({
                    status: StatusCodes.OK,
                    ok: true,
                    results: datas
                });
        } catch (error: any) {
            request.log.error(error);
            return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                .send({
                    status: StatusCodes.INTERNAL_SERVER_ERROR,
                    ok: false,
                    error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                });
        }
    })

    fastify.get('/getLookupCustomer/:id', async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;
        const id: number = req.params.id;
        try {
            let datas: any = await slotsModel.getLookupCustomer(db, id);

            return reply.status(StatusCodes.OK)
                .send({
                    status: StatusCodes.OK,
                    ok: true,
                    results: datas
                });
        } catch (error: any) {
            request.log.error(error);
            return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                .send({
                    status: StatusCodes.INTERNAL_SERVER_ERROR,
                    ok: false,
                    error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                });
        }
    })

}