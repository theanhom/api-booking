import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { CustomersModel } from '../../models/booking/customers';
import { ReservesModel } from '../../models/booking/reserves';
import { ServicesModel } from '../../models/booking/services';
import { HospitalsModel } from '../../models/booking/hospitals';
import { PeriodsModel } from '../../models/booking/periods';
import { UsersModel } from '../../models/booking/users';
import { ServiceTypesModel } from '../../models/booking/service_types';
import { SlotsModel } from '../../models/booking/slots';
import { ProfilesModel } from '../../models/booking/profiles';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const customersModel = new CustomersModel();
  const reservesModel = new ReservesModel();
  const servicesModel = new ServicesModel();
  const hospitalsModel = new HospitalsModel();
  const periodsModel = new PeriodsModel();
  const usersModel = new UsersModel();
  const serviceTypesModel = new ServiceTypesModel();
  const slotsModel = new SlotsModel();
  const profilesModel = new ProfilesModel();

  fastify.get('/customers/list', async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      let datas: any = await customersModel.list(db);

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })


  fastify.get('/getPersonReserve/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id: number = req.params.id;
    let customer: any = [];
    let slot: any = [];
    let service: any = [];
    let service_type: any = [];

    try {
      let datas: any = await reservesModel.getPersonReserve(db, id);

      if (datas.length > 0) {
        for (let v of datas) {
          customer = await customersModel.getByID(db, v.customer_id);
          if (customer.length > 0) {
            v.customer = customer;
          }
          slot = await slotsModel.getByID(db, v.slot_id);
          if (slot.length > 0) {
            v.slot = slot;
          }
          service_type = await serviceTypesModel.getByID(db, v.service_type_id);
          if (service_type.length > 0) {
            v.service_type = service_type;
          }
        }
      }

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.get('/getLookupByHospitalID/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id: number = req.params.id;

    try {
      let datas: any = await slotsModel.getLookupByHospitalID(db, id);

      if (datas.length > 0) {
        for (let v of datas) {
          let service = await servicesModel.getByID(db, v.service_id);
          console.log(service);

          if (service.length > 0) {
            v.service_name = service[0].service_name;
          }
          let service_type = await serviceTypesModel.getByID(db, v.service_type_id);
          if (service_type.length > 0) {
            v.service_type_name = service_type[0].service_type_name;
          }
          let period = await periodsModel.getByID(db, v.period_id);
          if (period.length > 0) {
            v.period_name = period[0].period_name;
          }
        }
      }

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })


  done();
}